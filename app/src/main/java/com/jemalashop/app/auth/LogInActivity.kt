package com.jemalashop.app.auth

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.jemalashop.app.R
import com.jemalashop.app.SharedPreferences
import com.jemalashop.app.SharedPreferences.USER_EMAIL
import com.jemalashop.app.SharedPreferences.USER_PASSWORD
import com.jemalashop.app.Tools
import com.jemalashop.app.extensions.setCheckMark
import com.jemalashop.app.extensions.setColor
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {


    var isChecked: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        readUserInfo()
        setTextSignUpTV()
        logInEmailET.addTextChangedListener(textWatcher)
    }

    private fun setTextSignUpTV() {
        signUpTV.setColor(
            getString(R.string.new_user),
            ContextCompat.getColor(this, R.color.textColor)
        )
        signUpTV.setColor(
            getString(R.string.sign_up),
            ContextCompat.getColor(this, R.color.colorPrimary)
        )
        signUpTV.setColor(
            getString(R.string.here),
            ContextCompat.getColor(this, R.color.textColor)
        )

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            emailValidation(s.toString())
        }

    }

    fun rememberMe(view: View) {
        isChecked = if (isChecked) {
            rememberMeImage.setImageResource(R.mipmap.ic_remember_me_checked)
            SharedPreferences.saveString(USER_EMAIL, logInEmailET.text.toString())
            SharedPreferences.saveString(USER_PASSWORD, logInPasswordET.text.toString())
            false
        } else {
            rememberMeImage.setImageResource(R.mipmap.ic_remember_me_unchecked)
            true
        }
    }

    private fun emailValidation(string: String) {
        logInEmailET.setCheckMark(Tools.isEmailValid(string), this)
    }


    private fun readUserInfo() {
        logInEmailET.setText(SharedPreferences.getString(USER_EMAIL))
        logInPasswordET.setText(SharedPreferences.getString(USER_PASSWORD))
    }

    fun signUp(view: View) {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    fun signIn(view: View) {
        if (logInEmailET.text!!.isEmpty() || logInPasswordET.text!!.isEmpty()) {
            Tools.initDialog(
                this,
                "Auth Error",
                "Please Fill All Fields"
            )
        } else {
            if (!Tools.isEmailValid(logInEmailET.text.toString())) {
                Tools.initDialog(
                    this, "Auth Error", "Enter a valid email"
                )
            } else {
                val intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }
        }
    }
}