package com.jemalashop.app.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.jemalashop.app.App
import com.jemalashop.app.R

fun EditText.setCheckMark(emailValid: Boolean, context: Context){
    var drawable: Drawable? = null
    if(emailValid)
        drawable = ContextCompat.getDrawable(context, R.drawable.check_mark_background)
    setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)

}

fun EditText.setTextWatcher(editTextWatcherListener: EditTextWatcherListener) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            editTextWatcherListener.onChange(p0, p1, p2, p3)
        }

    })
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun EditText.setOnFocusableListener() {
    val context = App.context
    setOnFocusChangeListener { _, hasFocus ->
        background = if (hasFocus)
            context.getDrawable(R.drawable.edit_text_shape_focused)
        else
            context.getDrawable(R.drawable.edit_text_shape_default)

    }
}

interface EditTextWatcherListener {
    fun onChange(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
}