package com.jemalashop.app

import android.content.Context

object SharedPreferences {

    const val USER_ID = "USER_ID"
    const val USER_EMAIL = "USER_EMAIL"
    const val USER_PASSWORD = "USER_PASSWORD"


    private val preference by lazy {
        App.instance.getContext().getSharedPreferences("USER", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        preference.edit()
    }

    fun saveString(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun getString(key: String) = preference.getString(key, "")

    fun removeString(key: String) {
        if (preference.contains(key))
            editor.remove(key)
        editor.commit()
    }

    fun clearAll() {
        editor.clear()
        editor.commit()
    }
}