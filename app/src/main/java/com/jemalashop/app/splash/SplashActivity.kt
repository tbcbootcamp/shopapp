package com.jemalashop.app.splash

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.jemalashop.app.R
import com.jemalashop.app.SharedPreferences
import com.jemalashop.app.auth.LogInActivity
import com.jemalashop.app.auth.SignUpActivity

class SplashActivity : AppCompatActivity() {

    private val handler = Handler()
    private val runnable = Runnable {
        openActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onStart() {
        super.onStart()
        postDelayed()
    }

    override fun onPause() {
        super.onPause()
        removeCallBack()
    }


    private fun openActivity() {
        val activity: Activity =
            if (SharedPreferences.getString(SharedPreferences.USER_ID)!!.isEmpty())
                LogInActivity()
            else
                DashboardActivity()

        val intent = Intent(this, activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun postDelayed() {
        handler.postDelayed(runnable, 2500)
    }

    private fun removeCallBack() {
        handler.removeCallbacks(runnable)
    }

}
